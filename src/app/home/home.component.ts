import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'mt-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  trained: any = 0;
  trainFlag: boolean = false;
  result: any;
  
  constructor(private appService: AppService) { }

  ngOnInit() {
  }

  onResultClick() {
    this.appService.requestResult().subscribe(data => this.result = data._body);

  }

  onTrainClick() {
    this.trainFlag = true;
    this.trained = 'not implemented yet';
    //this.appService.requestTraining().subscribe(data => this.trained = data);
  }
}
