import { SAP_API } from './app.api';
import { Http } from "@angular/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";

@Injectable()

export class AppService {
    constructor(private http: Http){ }

    requestResult(): Observable<any> {
        return this.http.get(`${SAP_API}/result`);
    }

    requestTraining(): Observable<any> {
        return this.http.get(`${SAP_API}/train`);
    }

}