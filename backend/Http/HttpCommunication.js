const express = require('express');
const router = require('express').Router();
const app = express();
const port = 3000;
const routes = require('./HttpRoutes');

class HttpApp {
    constructor() {
        this.initHttpApp();
    }

    initHttpApp() {
        new routes(app)
        app.use(router);
        app.listen(port, () => {
            console.log(`the app is listening on port ${port}`)
        });
    }
}

module.exports = HttpApp;
