//const model = require('../ML/model')

class HttpRoutes {
    constructor(app) {
        this.app = app;
        this.routes();
    }

    routes() {
        this.app.get('/result', function (req, res) {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.send(JSON.stringify(Math.ceil(Math.random() *8 + 1)))
        })

        // this.app.get('/train', function(req, res) {
        //     await model.loadData();
        //     this.setState({...this.state, trainingStarted: true});
        //     await model.train(this.onBatchProcessed, this.onEpochProcessed, this.onTrainingFinished);
        //     res.setHeader('Access-Control-Allow-Origin', '*');
        //     res.send(`model trained`);
        // })

    }

}

module.exports = HttpRoutes;