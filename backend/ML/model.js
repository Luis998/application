import {layers, sequential, train} from "@tensorflow/tfjs";
import {fromPixels, scalar, nextFrame} from '@tensorflow/tfjs';
import {MLData} from "./data";
const {conv2d, maxPooling2d, flatten, dense} = layers;

export class Model {

    constructor() {
        this._model = Model.build();
        this._data = new MLData();
      }
    
  static LEARNING_RATE = 0.15;
  static VALIDATION_SPLIT = 0.15;
  static BATCH_SIZE = 128;

  trainEpochs = 5;
  _model;
  _data;
  _trainBatchCount = 0;
  _valAcc;
  _trainData;
  _testData;
  _totalNumBatches;

  static build() {
    const model = sequential();
    model.add(conv2d({inputShape: [28, 28, 1], kernelSize: 3, filters: 16, activation: 'relu'}));
    model.add(maxPooling2d({poolSize: 2, strides: 2}));
    model.add(conv2d({kernelSize: 3, filters: 32, activation: 'relu'}));
    model.add(maxPooling2d({poolSize: 2, strides: 2}));
    model.add(conv2d({kernelSize: 3, filters: 32, activation: 'relu'}));
    model.add(flatten());
    model.add(dense({units: 64, activation: 'relu'}));
    model.add(dense({units: 10, activation: 'softmax'}));
    model.compile(
      {optimizer: MnistModelBuilder.optimizer, loss: 'categoricalCrossentropy', metrics: ['accuracy']}
    );
    return model;
  }

  static get optimizer() {
    return train.sgd(MnistModelBuilder.LEARNING_RATE);
  }

  async loadData() {
    await this._data.load();
    this._trainData = this._data.trainData;
    this._testData = this._data.testData;
    this._totalNumBatches = Math.ceil(
      this._trainData.xs.shape[0] * (1 - MnistModel.VALIDATION_SPLIT) / MnistModel.BATCH_SIZE
    ) * this.trainEpochs;
  }

  async train(onBatchProcessed, onEpochProcessed, onFinished) {
    await this._model.fit(
      this._trainData.xs,
      this._trainData.labels,
      {
        batchSize: MnistModel.BATCH_SIZE,
        validationSplit: MnistModel.VALIDATION_SPLIT,
        epochs: this.trainEpochs,
        callbacks: {
          onBatchEnd: (batch, logs) => this.onTrainingBatchEnd(batch, logs, onBatchProcessed),
          onEpochEnd: (epoch, logs) => this.onTrainEpochEnd(epoch, logs, onEpochProcessed)
        }
      }
    );
    const testResult = this._model.evaluate(this._testData.xs, this._testData.labels);
    onFinished(testResult);
  }

  predict(imageData) {
    let inputTensor = fromPixels(imageData, 1)
      .reshape([1, 28, 28, 1])
      .cast('float32')
      .div(scalar(255));
    const predictionResult = this._model.predict(inputTensor).dataSync();

    return predictionResult.indexOf(Math.max(...predictionResult));
  }

  async onTrainingBatchEnd(batch, logs, callback) {
    this._trainBatchCount++;
    const progress = this._trainBatchCount / this._totalNumBatches * 100;
    callback(progress, logs.loss, logs.acc);
    await nextFrame();
  }

  async onTrainEpochEnd(epoch, logs, callback) {
    this._valAcc = logs.val_acc;
    callback(this._trainBatchCount, logs.loss, logs.acc);
    await nextFrame();
  }
}
