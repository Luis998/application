import {tensor4d, tensor2d} from '@tensorflow/tfjs';

export const IMAGE_H = 28;
export const IMAGE_W = 28;
const IMAGE_SIZE = IMAGE_H * IMAGE_W;
const NUM_CLASSES = 10;
const NUM_DATASET_ELEMENTS = 60000;

const NUM_TRAIN_ELEMENTS = 55999;

const MNIST_IMAGES_SPRITE_PATH = './assets/train-images-idx3-ubyte';
const MNIST_LABELS_PATH = './assets/train-labels-idx1-ubyte';

export class MLData {

  async load() {
    const img = new Image();
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    const imgRequest = new Promise((resolve, reject) => {
      img.crossOrigin = '';
      img.onload = () => {
        img.width = img.naturalWidth;
        img.height = img.naturalHeight;

        const datasetBytesBuffer = new ArrayBuffer(NUM_DATASET_ELEMENTS * IMAGE_SIZE * 4);

        const chunkSize = 5000;
        canvas.width = img.width;
        canvas.height = chunkSize;

        for (let i = 0; i < NUM_DATASET_ELEMENTS / chunkSize; i++) {
          const datasetBytesView = new Float32Array(
            datasetBytesBuffer, i * IMAGE_SIZE * chunkSize * 4, IMAGE_SIZE * chunkSize
          );
          ctx.drawImage(img, 0, i * chunkSize, img.width, chunkSize, 0, 0, img.width, chunkSize);

          const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);

          for (let j = 0; j < imageData.data.length / 4; j++) {
            datasetBytesView[j] = imageData.data[j * 4] / 255;
          }
        }
        this.datasetImages = new Float32Array(datasetBytesBuffer);

        resolve();
      };
      img.src = MNIST_IMAGES_SPRITE_PATH;
    });

    const labelsRequest = fetch(MNIST_LABELS_PATH);
    const response = await Promise.all([imgRequest, labelsRequest]);
    const labelsResponse = response[1];

    this.datasetLabels = new Uint8Array(await labelsResponse.arrayBuffer());
    this.trainImages = this.datasetImages.slice(0, IMAGE_SIZE * NUM_TRAIN_ELEMENTS);
    this.testImages = this.datasetImages.slice(IMAGE_SIZE * NUM_TRAIN_ELEMENTS);
    this.trainLabels = this.datasetLabels.slice(0, NUM_CLASSES * NUM_TRAIN_ELEMENTS);
    this.testLabels = this.datasetLabels.slice(NUM_CLASSES * NUM_TRAIN_ELEMENTS);
  }

  get trainData() {
    const xs = tensor4d(this.trainImages, this.imagesTensorShape(this.trainImages.length));
    const labels = tensor2d(this.trainLabels, this.labelsTensorShape(this.trainLabels.length));
    return {xs, labels};
  }

  get testData() {
    let xs = tensor4d(this.testImages, this.imagesTensorShape(this.testImages.length));
    let labels = tensor2d(this.testLabels, this.labelsTensorShape(this.testLabels.length));
    return {xs, labels};
  }

  imagesTensorShape = (dim1) => [dim1 / IMAGE_SIZE, IMAGE_H, IMAGE_W, 1];
  labelsTensorShape = (dim1) => [dim1 / NUM_CLASSES, NUM_CLASSES]
}
