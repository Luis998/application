START THE APPLICATION

1. Open the project folder on the terminal;

2. Run npm i to install all the project dependencies;

3. Run npm start to start the frontend;

4. In another terminal, open the backend folder and run node main.js to start the backend;

5. The web page is available at localhost:4200.



Important: Is necessary to have installed node js to run the application

